from django.db import models
from django.utils import timezone


class Branch(models.Model):
    name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    branch_code = models.CharField(max_length=250)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    class Meta:
        verbose_name_plural = "Branches"

    def __str__(self):
        return self.name


class Bank(models.Model):
    name = models.CharField(max_length=250)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.name

class Client(models.Model):
    name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
    
class ClientManager(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.client)


class Account(models.Model):
    ACCOUNT_TYPES = [
        ('savings', 'Savings'),
        ('checking', 'Checking'),
    ]

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    open_date = models.DateField()
    account_type = models.CharField(max_length=10, choices=ACCOUNT_TYPES)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.get_account_type_display()} - {self.client.name}"



class Transfer(models.Model):
    sender_account = models.ForeignKey(
        Account, related_name='transfers_sent', on_delete=models.CASCADE, null=True)
    receiver_account = models.ForeignKey(
        Account, related_name='transfers_received', on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Transfer from {self.sender_account} to {self.receiver_account}"


class Withdraw(models.Model):
    amount = models.FloatField()
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        transfer = Transfer.objects.create(
            sender_account=self.account,
            receiver_account=None,
            amount=self.amount,
        )
        super().save(*args, **kwargs)

class Deposit(models.Model):
    amount = models.FloatField()
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    update_date = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        transfer = Transfer.objects.create(
            sender_account=None,
            receiver_account=self.account,
            amount=self.amount,
        )
        super().save(*args, **kwargs)
