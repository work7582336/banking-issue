from django.shortcuts import render, get_object_or_404
from django_filters import rest_framework as django_filters
from django.http import Http404
from rest_framework import generics, status, filters
from rest_framework.response import Response
from rest_framework.views import APIView
from datetime import datetime
from decimal import Decimal

from api.models import (
    Branch,
    Bank,
    ClientManager,
    Client,
    Account,
    Transfer,
    Withdraw,
    Deposit,
)
from api.serializers import (
    BranchSerializer,
    BankSerializer,
    ClientManagerSerializer,
    ClientSerializer,
    AccountSerializer,
    TransferSerializer,
    WithdrawSerializer,
    DepositSerializer,
)


class BranchesAPIView(generics.ListCreateAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer


class BranchDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer


class BanksAPIView(generics.ListCreateAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


class BankDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


class ClientsAPIView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientManagerListCreateAPIView(generics.ListCreateAPIView):
    queryset = ClientManager.objects.all()
    serializer_class = ClientManagerSerializer


class ClientManagerDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ClientManager.objects.all()
    serializer_class = ClientManagerSerializer


class AccountListAPIView(generics.ListAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


class AccountDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


class CreateAccountAPIView(APIView):
    def post(self, request):
        """
        Endpoint to create a new bank account 
        for a customer with an initial deposit amount.
        """
        data = request.data
        client_id = data.get('client')  # Get the client ID from the request data

        try:
            client = Client.objects.get(pk=client_id)
        except Client.DoesNotExist:
            return Response({"error": "Client does not exist."}, status=status.HTTP_400_BAD_REQUEST)


        bank = Bank.objects.get(pk=data['bank'])
        account_type = data['account_type']
        open_date = data['open_date']
        initial_deposit = float(data.get('initial_deposit', 100.0))

        # Set the initial deposit to 100 if it's not provided or <= 0
        if initial_deposit <= 0:
            initial_deposit = 100.0

        account = Account.objects.create(
            client=client,
            bank=bank,
            account_type=account_type,
            open_date=open_date,
            create_date=datetime.now(),
            update_date=datetime.now(),
        )

        # Perform an initial deposit
        Transfer.objects.create(
            sender_account=account,
            receiver_account=account,
            amount=initial_deposit,
            create_date=datetime.now(),
            update_date=datetime.now(),
        )

        account.balance += initial_deposit
        account.save()

        serializer = AccountSerializer(account)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TransferAmountAPIView(APIView):
    def post(self, request):
        """
        Endpoint to transfer amounts between any two accounts,
        including those owned by different customers.
        """
        data = request.data
        sender_account_id = data.get('sender_account')
        receiver_account_id = data.get('receiver_account')
        amount = Decimal(data.get('amount', '0.0'))

        # Retrieve sender and receiver accounts
        sender_account = get_object_or_404(Account, pk=sender_account_id)
        receiver_account = get_object_or_404(Account, pk=receiver_account_id)

        if sender_account.balance < amount:
            return Response({"detail": "Insufficient funds."}, status=status.HTTP_400_BAD_REQUEST)

        # Create a transfer record without specifying create_date and update_date
        transfer = Transfer.objects.create(
            sender_account=sender_account,
            receiver_account=receiver_account,
            amount=amount,
        )

        # Update account balances
        sender_account.balance -= amount
        sender_account.save()
        receiver_account.balance += amount
        receiver_account.save()

        serializer = TransferSerializer(transfer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class AccountBalanceAPIView(APIView):
    def get(self, request, pk):
        """
        Endpoint to retrieve the balance for a given account.
        """
        account = get_object_or_404(Account, pk=pk)
        balance = account.balance

        return Response({"balance": balance}, status=status.HTTP_200_OK)


class TransferHistoryAPIView(APIView):
    def get(self, request, pk):
        """
        Endpoint to retrieve transfer history for a given account.
        """
        account = get_object_or_404(Account, pk=pk)

        sent_transfers = Transfer.objects.filter(sender_account=account)
        received_transfers = Transfer.objects.filter(receiver_account=account)

        sent_serializer = TransferSerializer(sent_transfers, many=True)
        received_serializer = TransferSerializer(received_transfers, many=True)

        balance = account.balance

        data = {
            "balance": balance,
            "sent_transfers": sent_serializer.data,
            "received_transfers": received_serializer.data,
        }

        return Response(data, status=status.HTTP_200_OK)


class WithdrawAPIView(APIView):
    def post(self, request, pk):
        """
        Endpoint to make a withdrawal from a specific account.
        """
        account = get_object_or_404(Account, pk=pk)
        data = request.data
        amount = Decimal(data.get('amount', 0.0))

        if amount <= 0:
            return Response({"detail": "Withdrawal amount must be greater than zero."}, status=status.HTTP_400_BAD_REQUEST)

        # Ensure the account has sufficient balance for withdrawal
        if account.balance < amount:
            return Response({"detail": "Insufficient funds for withdrawal."}, status=status.HTTP_400_BAD_REQUEST)

        # Create a withdrawal record
        withdrawal = Withdraw.objects.create(
            account=account,
            amount=amount,
        )

        # Create a transfer record for the withdrawal (sender is the account itself)
        transfer = Transfer.objects.create(
            sender_account=account,
            receiver_account=None,
            amount=amount,
        )

        # Update the account balance
        account.balance -= amount
        account.save()

        serializer = WithdrawSerializer(withdrawal)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class DepositAPIView(APIView):
    def post(self, request, pk):
        """
        Endpoint to make a deposit to a specific account.
        """
        account = get_object_or_404(Account, pk=pk)
        data = request.data
        amount = Decimal(data.get('amount', 0.0))

        if amount <= 0:
            return Response({"detail": "Deposit amount must be greater than zero."}, status=status.HTTP_400_BAD_REQUEST)

        # Create a deposit record
        deposit = Deposit.objects.create(
            account=account,
            amount=amount,
        )

        # Create a transfer record for the deposit
        transfer = Transfer.objects.create(
            sender_account=None,
            receiver_account=account,
            amount=amount,
        )

        # Update the account balance
        account.balance += amount
        account.save()

        serializer = DepositSerializer(deposit)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
