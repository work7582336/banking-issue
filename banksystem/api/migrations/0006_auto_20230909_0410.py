# Generated by Django 3.2.9 on 2023-09-09 04:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_auto_20230909_0407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='bank',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='branch',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='client',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='clientmanager',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='deposit',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='transfer',
            name='delete_date',
        ),
        migrations.RemoveField(
            model_name='withdraw',
            name='delete_date',
        ),
    ]
