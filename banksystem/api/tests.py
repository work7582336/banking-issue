from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from datetime import datetime
from api.models import *
class BranchesAPITestCase(APITestCase):
    def test_list_branches(self):
        url = reverse('api:branches-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_branch(self):
        url = reverse('api:branches-list')
        data = {
            "name": "New Branch",
            "address": "123 New St",
            "branch_code": "0010",
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_retrieve_branch(self):
        branch = Branch.objects.create(name="Test Branch", address="456 Test St")
        url = reverse('api:branch-detail', args=[branch.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class BanksAPITestCase(APITestCase):
    def test_list_banks(self):
        url = reverse('api:banks-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class ClientsAPITestCase(APITestCase):
    def test_list_clients(self):
        url = reverse('api:clients-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
