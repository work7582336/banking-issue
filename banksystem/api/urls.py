from django.urls import path

from api.views import (
    BranchesAPIView,
    BranchDetailAPIView,
    BanksAPIView,
    BankDetailAPIView,
    ClientsAPIView,
    ClientDetailAPIView,
    CreateAccountAPIView,
    AccountListAPIView,
    TransferAmountAPIView,
    AccountBalanceAPIView,
    TransferHistoryAPIView,
    AccountDetailAPIView,
    DepositAPIView,
    WithdrawAPIView,
    ClientManagerListCreateAPIView,
    ClientManagerDetailAPIView,
)

app_name = 'api'

urlpatterns = [
    path('branches/', BranchesAPIView.as_view(), name='branches-list'),
    path('branch/<int:pk>/', BranchDetailAPIView.as_view(), name='branch-detail'),
    path('banks/', BanksAPIView.as_view(), name='banks-list'),
    path('bank/<int:pk>/', BankDetailAPIView.as_view(), name='bank-detail'),
    path('clients/', ClientsAPIView.as_view(), name='clients-list'),
    path('client/<int:pk>/', ClientDetailAPIView.as_view(), name='client-detail'),
    path('create_account/', CreateAccountAPIView.as_view(), name='create-account'),
    path('accounts/', AccountListAPIView.as_view(), name='accounts'),
    path('account/<int:pk>/', AccountDetailAPIView.as_view(), name='account-detail'),
    path('transfer_amount/', TransferAmountAPIView.as_view(), name='transfer-amount'),
    path('account_balance/<int:pk>/', AccountBalanceAPIView.as_view(), name='account-balance'),
    path('transfer_history/<int:pk>/', TransferHistoryAPIView.as_view(), name='transfer-history'),
    path('deposit/<int:pk>/', DepositAPIView.as_view(), name='deposit'),
    path('withdraw/<int:pk>/', WithdrawAPIView.as_view(), name='withdraw'), 
    path('clientmanagers/', ClientManagerListCreateAPIView.as_view(), name='clientmanager-list'),
    path('clientmanager/<int:pk>/', ClientManagerDetailAPIView.as_view(), name='clientmanager-detail'),
    path('clients/', ClientDetailAPIView.as_view(), name='client-list'),

]
